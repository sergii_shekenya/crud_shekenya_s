package com.shekenya.persistence;

import com.shekenya.context.ApplicationContext;
import com.shekenya.datasctructure.Database;

import java.io.*;

/**
 * Created by Cinderella Kant on 12.02.14.
 */
public class Persistence {

    public Persistence() {
        FileInputStream fileInputStream = null;
        ObjectInputStream objectInputStream = null;
        try {
            fileInputStream = new FileInputStream(new File("").getAbsolutePath() + "/resources/database.dtb");
            objectInputStream = new ObjectInputStream(fileInputStream);
            Database database = (Database) objectInputStream.readObject();
            ApplicationContext.getController().setDatabase(database);
            ApplicationContext.setDatabase(database);
            objectInputStream.close();
            fileInputStream.close();
        } catch (Exception e) {

        }
    }

    public void persist() {
        Database database = ApplicationContext.getDatabase();
        FileOutputStream fileOutputStream = null;
        ObjectOutputStream objectOutputStream;

        try {
            fileOutputStream = new FileOutputStream(new File("").getAbsolutePath() + "/resources/database.dtb");
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(database);
            objectOutputStream.flush();
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }
}
