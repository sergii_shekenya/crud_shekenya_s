package com.shekenya.control;

/**
 * Created by Cinderella Kant on 11.02.14.
 */
public interface Query<E> {
    public void invoke();

    public E result();
}
