package com.shekenya.control;

import com.shekenya.context.ApplicationContext;
import com.shekenya.datasctructure.Database;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Cinderella Kant on 11.02.14.
 */
public class DatabaseController extends Thread {
    private BufferedReader reader;


    private Database database;

    public DatabaseController() {
        reader = new BufferedReader(new InputStreamReader(System.in));
        database = new Database();
        ApplicationContext.setDatabase(database);
    }

    @Override
    public void run() {
        System.out.println("Please, type command or -h for help");
        while (true) {
            String s = null;
            try {
                s = reader.readLine();
                Query<String> query = new CRUDQuery(s);
                query.invoke();
                System.out.println(query.result());
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            if (this.isInterrupted()) {
                return;
            }
        }
    }

    public Database getDatabase() {
        return database;
    }

    public void setDatabase(Database database) {
        this.database = database;
    }
}
