package com.shekenya.control;

import com.shekenya.context.ApplicationContext;
import com.shekenya.datasctructure.Contact;
import com.shekenya.datasctructure.Database;
import com.shekenya.enums.QueryType;
import com.shekenya.exceptions.ElementNotFoundException;

import java.io.*;

/**
 * Created by Cinderella Kant on 12.02.14.
 */
public class CRUDQuery implements Query<String> {
    private String[] splitQuery;
    private QueryType queryType;
    private String result;
    private IllegalStateException stateException = new IllegalStateException("Query is invalid");
    Database database;

    public CRUDQuery(String query) {
        splitQuery = query.split(" ");
        String type = splitQuery[0].toLowerCase();
        queryType = QueryType.getQueryTypeByLabel(type);
        database = ApplicationContext.getDatabase();
        result="";
    }

    @Override
    public void invoke() {
        if (queryType == QueryType.CREATE) {
            create();
        }
        if (queryType == QueryType.RETRIEVE) {
            retrieve();
        }
        if (queryType == QueryType.UPDATE) {
            update();
        }
        if (queryType == QueryType.DELETE) {
            delete();
        }
        if (queryType == QueryType.SHUTDOWN) {
            shutdown();
        }
        if (queryType == QueryType.HELP) {
            BufferedReader reader = null;
            try {
                File file = new File("");
                reader = new BufferedReader(new FileReader(file.getAbsolutePath() + "\\resources\\messages.list"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            while (true) {
                try {
                    String s = reader.readLine();
                    if (s == null) break;
                    result += s + "\n";
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //TODO need to re-implement invoking of query logic to extend OOP approach
    private void create() {
        if (splitQuery.length != 3) throw stateException;

        database.add(splitQuery[1], splitQuery[2]);
        result = String.format("One new contact created: %s %s", splitQuery[1], splitQuery[2]);
    }

    private void retrieve() {
        if (splitQuery.length != 2) throw stateException;

        try {
            Contact contact = database.get(splitQuery[1]);
            result = String.format("Name: %s\nTelephone: %s", contact.getName(), contact.getTelephone());
        } catch (ElementNotFoundException e) {
            result = "Contact is not found";
        }
    }

    private void update() {
        if (splitQuery.length != 3) throw stateException;

        try {
            database.set(splitQuery[1], splitQuery[2]);
            result = String.format("Contact \"%s\" is updated. New telephone number: %s", splitQuery[1], splitQuery[2]);
        } catch (ElementNotFoundException e) {
            result = "Contact is not found";
        }
    }

    private void delete() {
        if (splitQuery.length != 2) throw stateException;
        try {
            database.remove(splitQuery[1]);
            result = String.format("Contact \"%s\" is deleted", splitQuery[1]);
        } catch (ElementNotFoundException e) {
            result = "Contact is not found";
        }
    }

    private void shutdown() {
        if (splitQuery.length != 1) throw stateException;
        result = "Application is shutdown";
        ApplicationContext.getController().interrupt();
    }

    @Override
    public String result() {
        return this.result;
    }
}
