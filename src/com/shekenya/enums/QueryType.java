package com.shekenya.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Cinderella Kant on 11.02.14.
 */
public enum QueryType {
    CREATE("create"),
    RETRIEVE("retrieve"),
    UPDATE("update"),
    DELETE("delete"),
    SHUTDOWN("shutdown"),
    HELP("-h");
    private static Map<String, QueryType> queryTypeMap;

    static {
        queryTypeMap = new HashMap<String, QueryType>();
        queryTypeMap.put(CREATE.getLabel(), CREATE);
        queryTypeMap.put(RETRIEVE.getLabel(), RETRIEVE);
        queryTypeMap.put(UPDATE.getLabel(), UPDATE);
        queryTypeMap.put(DELETE.getLabel(), DELETE);
        queryTypeMap.put(SHUTDOWN.getLabel(), SHUTDOWN);
        queryTypeMap.put(HELP.getLabel(), HELP);

    }

    private String label;

    QueryType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static QueryType getQueryTypeByLabel(String str) {
        return queryTypeMap.get(str.toLowerCase());
    }
}
