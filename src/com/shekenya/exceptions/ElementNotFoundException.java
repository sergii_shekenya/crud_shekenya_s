package com.shekenya.exceptions;

/**
 * Created by Cinderella Kant on 11.02.14.
 */
public class ElementNotFoundException extends Exception {
    public ElementNotFoundException(String message) {
        super(message);
    }
}
