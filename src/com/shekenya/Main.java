package com.shekenya;

import com.shekenya.context.ApplicationContext;
import com.shekenya.control.DatabaseController;
import com.shekenya.datasctructure.Contact;
import com.shekenya.datasctructure.Database;
import com.shekenya.exceptions.ElementNotFoundException;
import com.shekenya.persistence.Persistence;



/**
 * Created by Cinderella Kant on 11.02.14.
 */
public class Main {
    public static void main(String... args) {
        DatabaseController controller = new DatabaseController();
        ApplicationContext.setController(controller);
        Persistence persistence = new Persistence();
        ApplicationContext.setPersistence(persistence);

        controller.start();
    }

    public static void test() {
        Database database = new Database();
        Database.Basket basket = null;
        try {
            basket = database.search("e");
        } catch (ElementNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(basket);
    }

}
