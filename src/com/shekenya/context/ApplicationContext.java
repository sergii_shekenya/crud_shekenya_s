package com.shekenya.context;

import com.shekenya.control.DatabaseController;
import com.shekenya.datasctructure.Database;
import com.shekenya.persistence.Persistence;

/**
 * Created by Cinderella Kant on 12.02.14.
 */
public class ApplicationContext {
    private static DatabaseController controller;
    private static Database database;
    private static Persistence persistence;
    public static DatabaseController getController() {
        return controller;
    }

    public static void setController(DatabaseController controller) {
        ApplicationContext.controller = controller;
    }

    public static Database getDatabase() {
        return database;
    }

    public static void setDatabase(Database database) {
        ApplicationContext.database = database;
    }

    public static Persistence getPersistence() {
        return persistence;
    }

    public static void setPersistence(Persistence persistence) {
        ApplicationContext.persistence = persistence;
    }
}
