package com.shekenya.datasctructure;

import java.io.Serializable;

/**
 * Created by Cinderella Kant on 11.02.14.
 */
public class Contact implements Serializable {
    private String name;
    private String telephone;
    private Boolean isDeleted = false;

    public Contact(String name, String telephone) {
        this.name = name;
        this.telephone = telephone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
}
