package com.shekenya.datasctructure;

import com.shekenya.context.ApplicationContext;
import com.shekenya.exceptions.ElementNotFoundException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Cinderella Kant on 11.02.14.
 */
public class Database implements Serializable {
    private List<Basket> baskets;

    public Database() {
        baskets = new ArrayList<Basket>();
    }

    private void sort() {
        Collections.sort(baskets);
    }

    public Basket search(String str) throws ElementNotFoundException {
        int index = Collections.binarySearch(baskets, new Basket(str));
        if (index < 0) throw new ElementNotFoundException("Element not found");
        return baskets.get(index);
    }

    public void add(String name, String telephone) {
        Basket basket = null;
        try {
            basket = search(getArticle(name));
        } catch (ElementNotFoundException e) {

        }
        if (basket == null) {
            create(name, telephone);
        } else {
            basket.add(new Contact(name, telephone));
        }
        ApplicationContext.getPersistence().persist();
    }

    private void create(String name, String telephone) {
        Basket basket = new Basket(getArticle(name));
        basket.add(new Contact(name, telephone));
        baskets.add(basket);
        sort();
    }

    public Contact get(String name) throws ElementNotFoundException {
        Basket basket = search(getArticle(name));
        Contact contact = basket.get(name);
        return contact;
    }

    public void set(String name, String telephone) throws ElementNotFoundException {
        Basket basket = search(getArticle(name));
        Contact contact = basket.get(name);
        contact.setTelephone(telephone);
        ApplicationContext.getPersistence().persist();
    }

    public void remove(String name) throws ElementNotFoundException {
        Basket basket = search(getArticle(name));
        Contact contact = basket.get(name);
        contact.setDeleted(true);
        ApplicationContext.getPersistence().persist();
    }

    private String getArticle(String str) {
        return str.toLowerCase().charAt(0) + "";
    }

    public static class Basket implements Comparable<Basket>, Serializable {

        private String article;

        private List<Contact> contactList;

        private Basket(String article) {
            this.article = article;
            contactList = new ArrayList<Contact>();
        }

        public void add(Contact contact) {
            contactList.add(contact);
        }

        public Contact get(String name) throws ElementNotFoundException {
            Contact contact = null;
            for (Contact c : contactList) {
                if (c.getName().equals(name) && !c.isDeleted()) {
                    contact = c;
                }
            }
            if (contact == null) {
                throw new ElementNotFoundException("Element not Found");
            }
            return contact;
        }

        public List<Contact> getContactList() {
            return contactList;
        }

        public String getArticle() {
            return article;
        }

        public void setArticle(String article) {
            this.article = article;
        }

        @Override
        public int compareTo(Basket o) {
            return article.compareTo(o.getArticle());
        }
    }


}
